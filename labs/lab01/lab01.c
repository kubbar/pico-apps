#include "pico/stdlib.h"

/**
   @brief EXAMPLE - BLINK_C
          Simple example to initialise the built-in LED on
          the Raspberry Pi Pico and then flash it forever.
          The sub routine function is making it glow after a delay and this runs forever
   @return int  Application return code (zero for success).
*/

void subroutine(const uint LED_PIN, const uint LED_DELAY)
{
  while (true) {

    // Making the LED glow and then a delay period
    gpio_put(LED_PIN, 1);
    sleep_ms(LED_DELAY);

    // Turning the LED off and then the delay period 
    gpio_put(LED_PIN, 0);
    sleep_ms(LED_DELAY);

  }
}
int main() {

  // Specify the PIN number and sleep delay
  const uint LED_PIN   =  25;
  const uint LED_DELAY = 500;

  // Setup the LED pin as an output.
  gpio_init(LED_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);

  // Do forever...
  subroutine(LED_PIN, LED_DELAY);

  // Should never get here due to infinite while-loop.
  return 0;

}