//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     
#include "pico/double.h"  




void wallis_product_algorithm_double(double total_wallis){
    double unit1, unit2; 

    //This is the wallis product algorithm

    for (double i = 1; i <= 100000; i++){
        unit1 = (2 * i) / ( (2 * i) - 1);
        unit2 = (2 * i) / ( (2 * i) + 1);

        total_wallis *= unit1 * unit2;
    }

    //This print the value of pi and approximation error
    double calculated_PI2 = total_wallis * 2;

    printf("Double precision floating-point Pi = %.15f\n", calculated_PI2);

    double comp_pi2 = 3.14159265359; 
    double approx_error2 = ( ( (calculated_PI2 - comp_pi2) / comp_pi2) );

    printf("Approximation Error Double = %.15f\n", abs(approx_error2));

}  

void wallis_product_algorithm_float(float total_wallis){
    float unit1, unit2; 

    //wallis product algoruthm
    for (float i = 1; i <= 100000; i++){
        unit1 = (2 * i) / ( (2 * i) - 1);
        unit2 = (2 * i) / ( (2 * i) + 1);

        total_wallis *= unit1 * unit2;
    }

    float calculated_PI = total_wallis * 2;
    printf("Single precision floating-point Pi= %.7f\n", calculated_PI);

    float comp_pi = 3.14159265359; 

    //approximation error is calculated here

    float approximation_error = ( ( (calculated_PI - comp_pi) / comp_pi ) );

    printf("Approximation Error Float = %.7f\n", abs(approximation_error));

}


int main() {

#ifndef WOKWI
    stdio_init_all();
#endif
    float total_float = 1.0;
    double total_double = 1.0;
    printf("Calculating Pi using Single Precision Floating-Point!\n"); 
    wallis_product_algorithm_float(total_float);
    printf("Calculating Pi using Double Precision Floating-Point!\n");
    wallis_product_algorithm_double(total_double);
    return 0;
}
